describe('Test de Login', () => {
  const serverDomain = 'staging-frontv3.smood.ch'
  //Entrer sur le site
  it('Teste de  Page Login', () => {
        cy.visit('https://staging-frontv3.smood.ch/fr')
        cy.get('[data-cy="Home_Button_Connection"]').click()
    })
  //Créer son compte
  it('Test boutton Connexion', () => {
       cy.get('button[id="createAccountButton"]').click({force: true})
     })

   // Remplier formulaire d'inscription 
  it('Test Formulaire', () => {   
    // Cliquer sur boutton inscription pour faire appara
       cy.get('button[id="register"]').click({force: true})
       cy.get('#register-firstname').should('not.empty');
       cy.get('#register-lastname').should('not.empty');
       cy.get('#register-email').should('not.empty');
       cy.get('#register-phone-number').should('not.empty');
       cy.get('#register-password > .gbx-input > .gbx-input__input').should('not.empty');
       cy.get('#register-confirm-password > .gbx-input > .gbx-input__input').should('not.empty');
       cy.get('#register-cgu').check()
})
  it('Test envoi SMS confirmation', () => {
    const serverId = 'abcd1234'
    
      //creation de nouveau serveur
      it('.mailosaurCreateServer creattion de nouveau serveur', (done) => {
        cy.mailosaurCreateServer({
          name: serverName
        }).then((server) => {
          createdServer = server;
          expect(createdServer.id).to.be.ok
          expect(createdServer.name).to.equal(serverName)
          expect(createdServer.users).to.be.an('array')
          expect(createdServer.messages).to.be.a('number')
          done()
        });
      });

    cy.mailosaurGetMessage(serverId, {
      sentTo: '+261324253961'
     }).then(sms => {
      expect(sms.text.body).to.equal()
     })
  })
})